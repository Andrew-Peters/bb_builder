sudo apt install python3 python3-pip
pip install numpy

cp -r moltemplate_2022-7-06 $HOME/moltemplate
mkdir $HOME/BB_Builder
cp * $HOME/BB_Builder
mkdir -p $HOME/.local/bin/
cp BB_Builder.py $HOME/.local/bin/BB_Builder.py
echo 'export PYTHONPATH="$HOME/.local/bin:$PYTHONPATH"' >> ~/.bashrc
#echo 'export PATH="$PATH:$HOME/moltemplate/moltemplate"' >> ~/.bashrc
#echo 'export PATH="$PATH:$HOME/moltemplate/moltemplate/scripts"' >> ~/.bashrc
#echo 'export PYTHONPATH="$PYTHONPATH:$HOME/BB_Builder"' >> ~/.bashrc
source ~/.bashrc

