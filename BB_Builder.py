import numpy
import os

Nsc= input("Side Chain Lengths Eg. , for a 8-2-4 triblock, enter 8,2,4 : ")
Nsc_a=numpy.fromstring(Nsc, dtype=int, sep=',')

Nbb= input("Backbone Lengths Eg. if the 8-mer block has 5 mers, the 2-mer block has 3 mers, and the 4-mer block has 10 mers, enter 5,3,10 : ")

Nbb_a=numpy.fromstring(Nbb, dtype=int, sep=',')

print(Nsc_a)
print(Nbb_a)

#check to make sure Nbb_a and Nsc_a are the same length
if len(Nsc_a)!=len(Nbb_a):
	error("Length of Side Chain Array and Backbone Array are not Equal!")


Ngl=numpy.zeros(numpy.sum(Nbb_a))
counter=0;

for j in range(len(Nsc_a)):
	for i in range(Nbb_a[j]):
		Ngl[counter]=Nsc_a[j]
		counter=counter+1

fid2 = open("BB.lt","w")

nbackbone=sum(Nbb_a)
    
#headings
fid2.write('#import all require files\n')
fid2.write('import "Ph_Init.lt"  #Initiator for backbone\n')
fid2.write('import "Term.lt"  #Terminator for backbone\n')
fid2.write('import "GraftMon_Sty1.lt"  #Graft Monomer\n')
fid2.write('import "GraftMon_Sty1_1.lt"  #Graft Monomer\n')
fid2.write('import "GraftTerm_Sty1.lt"  #Graft Terminator\n')
fid2.write('import "Mon_Norb1.lt"  #Norb Monomer for 2\n')
fid2.write('\n')

#add backbone monomers
fid2.write('BB inherits OPLSAA_MODPS {\n')
fid2.write('create_var {$mol}  # optional:force all monomers to share the same molecule-ID\n')
fid2.write('monomers[0]=new Ph1 #Initial part of the backbone\n')
fid2.write('monomers[1-' + str(nbackbone) + '] = new Mon_Norb1 [' + str(nbackbone) + '].move(5,0,0)  #create backbone monomers, move and rotate\n')
fid2.write('monomers[1-' + str(nbackbone) + '].move(5,0,0) #move noninitiators\n')
fid2.write('monomers[' + str(nbackbone+1) + ']=new Term.move(' +  str(5+5*nbackbone) + ',0,0) #move terminator ot end\n')
fid2.write('\n')

# Add Grafts
for i in range(nbackbone):
	L=int(Ngl[i])
	if L<1:
		error("Side chain length less than one!")
	elif L>2:
		fid2.write('graft' + str(i+1) + '[0]=new GraftMon_Sty1_1.move(0,4,0) #add first graft\n')
		fid2.write('graft' + str(i+1) + '[1-' + str(L-1) + ']=new GraftMon_Sty1 [' + str(L-1) + '].move(0,-4,0) #add grafts\n')
		fid2.write('graft' + str(i+1) + '[' + str(L) + ']=new GraftTerm_Sty1.move(0,' + str(-4*L) + ',0) # add terminal graft monomer\n')
		fid2.write('graft' + str(i+1) + '[0-' + str(L) + '].move(' + str(5*(i+1)) + ',-8,0)#move grafts to correct backbone location\n')
		fid2.write('\n')
	elif L==2:
		fid2.write('graft' + str(i+1) + '[0]=new GraftMon_Sty1_1.move(0,0,0) #add first graft\n')
		fid2.write('graft' + str(i+1) + '[1]=new GraftMon_Sty1.move(0,-4,0) #add grafts\n')
		fid2.write('graft' + str(i+1) + '[' + str(L) + ']=new GraftTerm_Sty1.move(0,' + str(-4*L) + ',0) # add terminal graft monomer\n')
		fid2.write('graft' + str(i+1) + '[0-' + str(L) + '].move(' + str(5*(i+1)) + ',-4,0)#move grafts to correct backbone location\n')
		fid2.write('\n')
	else:
		error("Side Chain Length Defined Incorrectly")
	fid2.write('\n')


#add backbone bonds
fid2.write("write('Data Bond List') {\n")
fid2.write('$bond:b1  $atom:monomers[0]/c7 $atom:monomers[1]/c1\n')
counter=2;
for i in range(nbackbone):
	fid2.write('$bond:b' + str(counter) + '  $atom:monomers[' + str(i+1) + ']/c6 $atom:monomers[' + str(i+2) + ']/c1\n')
	counter=counter+1;

#add graft bonds
for i in range(nbackbone):
	L=int(Ngl[i])
	for j in range(L+1):
		j=j+1
		if j==1:
			fid2.write('$bond:g' + str(i+1) + '_' + str(j-1) + '   $atom:graft' + str(i+1) + '[' + str(j-1) + ']/c7 $atom:monomers[' + str(i+1) + ']/o_coo2\n')
		elif j<=L:
			fid2.write('$bond:g' + str(i+1) + '_' + str(j-1) + '   $atom:graft' + str(i+1) + '[' + str(j-1) + ']/c7 $atom:graft' + str(i+1) + '[' + str(j-2) + ']/c8\n')
		elif j==L+1:
			fid2.write('$bond:g' + str(i+1) + '_' + str(j-1) + '   $atom:graft' + str(i+1) + '[' + str(j-1) + ']/c1 $atom:graft' + str(i+1) + '[' + str(j-2) + ']/c8\n')

fid2.write('\n')
fid2.write('} #Bonds end\n')
fid2.write('\n} #BB \n')
fid2.close()
os.system('export PATH="$PATH:$HOME/moltemplate/moltemplate";export PATH="$PATH:$HOME/moltemplate/moltemplate/scripts";cp BB.lt $HOME/BB_Builder/BB.lt;CURDIR=$PWD;cd $HOME/BB_Builder;moltemplate.sh system.lt;cd $CURDIR;cp $HOME/BB_Builder/system.data system.data;cp $HOME/BB_Builder/system.in.charges system.in.charges;cp $HOME/BB_Builder/system.in.init system.in.init;cp $HOME/BB_Builder/system.in.settings system.in.settings;cp $HOME/BB_Builder/run.in.EXAMPLE run.in.EXAMPLE;')

